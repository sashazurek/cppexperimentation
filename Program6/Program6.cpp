/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #6                 *
 * Due Date: March 26th, 2019             *
 * Last Modified: March 23rd, 2019        *
 ******************************************/

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include "Program6.h"

using namespace std;

// Finds a specific sensor, then prints it.
bool printSensor(LLnode* head, LLnode data)
{
    LLnode * hike = head;
    LLnode * trail = NULL;
    int i, padding;

    // Ensure there are items to print. If there are not, bail.
    if (hike == NULL)
    {
        cout << "ERROR: There are no items to remove.\n\n";
        return false;
    }
    else
    {
        while (hike -> next != NULL && hike -> sensorID != data.sensorID)
        {
            trail = hike;
            hike = trail -> next;
        }
        if (hike -> next == NULL)
        {
            cout << "ERROR: Specified sensor not found.\n\n";
            return false;
        }
        else
        {
            cout << "Sensor\tForest\t\t\tTemp\tHumidity\tLight\n";
            cout << hike -> sensorID;
            cout << "\t" << hike -> forest;
            padding = strlen(hike -> forest);
            padding = ceil((forestNameLen - 1 - padding) / 8.0);
            for (i = 1; i < padding; i++)
            {
                cout << "\t";
            }
            cout << hike -> temperature;
            cout << "\t" << (hike -> humidity) * 100 << "%";
            cout << "\t\t" << hike -> lumens << endl;   
        }
    }
    cout << endl;
    return true;
}

// Finds a specific sensor, then updates it.
bool updateSensor(LLnode *head, LLnode data)
{
    LLnode * hike = head;
    LLnode * trail = NULL;

    // Bail if there are no items in the list.
    if (hike == NULL)
    {
        cout << "ERROR: There are no items to update.\n\n";
        return false;
    }
    else
    {
        // Find the specified sensor
        while (hike -> next != NULL && hike -> sensorID != data.sensorID)
        {
            trail = hike;
            hike = trail -> next;
        }
        if (hike -> next == NULL)
        {
            cout << "ERROR: Specified sensor not found.\n\n";
            return (false);
        }
        else
        {
            hike -> temperature = data.temperature;
            hike -> humidity = data.humidity;
            hike -> lumens = data.lumens;
        }
    }
    return true;
}

// Finds a specific sensor, then removes it.
bool remove(LLnode* &head, LLnode data)
{
    LLnode * hike = head;
    LLnode * trail = NULL;
    // Bail if there are no items in the list.
    if (hike == NULL)
    {
        cout << "ERROR: There are no items to remove.\n\n";
        return false;
    }
    else
    {
        // Find the specified sensor
        while (hike -> next != NULL && hike -> sensorID != data.sensorID)
        {
            trail = hike;
            hike = trail -> next;
        }
        if (hike -> next == NULL)
        {
            cout << "ERROR: Specified sensor not found.\n\n";
            return false;
        }
        else
        {
            hike = hike -> next;
            delete(trail -> next);
            trail -> next = hike;
        }
    }
    return true;
}

// Moves through the linked list, printing all values.
// Utilizes cmath library to assist in proper formatting
void printAll(LLnode* head)
{
    LLnode* hike = head;
    int padding, i;

    cout << "Sensor\tForest\t\t\tTemp\tHumidity\tLight\n";
    while (hike != NULL)
    {
        cout << hike -> sensorID;
        cout << "\t" << hike -> forest;
        padding = strlen(hike -> forest);
        padding = ceil((forestNameLen - 1 - padding) / 8.0);
        for (i = 1; i < padding; i++)
        {
            cout << "\t";
        }
        cout << hike -> temperature;
        cout << "\t" << (hike -> humidity) * 100 << "%";
        cout << "\t\t" << hike -> lumens << endl;
        hike = hike -> next;
    }
    cout << endl;
    return;
}

// Function to insert data in the list
bool insert(LLnode* &head, LLnode data)
{
    LLnode * insert = head;
    LLnode * trail = NULL;
    
    if (insert == NULL)
    {
		// Empty case
		head = new(LLnode);
		*head = data;
		head -> next = NULL;
	}
	else
	{
		// Search for our insertion spot
		while (insert != NULL && insert -> sensorID < data.sensorID)
		{
			trail = insert;
			insert = insert -> next;
		}

		if (insert != NULL && insert -> sensorID == data.sensorID)
		{
			// Duplicate ID
            cout << "ERROR: ID " << data.sensorID <<
                " already exists.\n\n";
			return false;
		}
		else if (trail == NULL)
		{
			// Insert before the head
			insert = new(LLnode);
			*insert = data;
			insert -> next = head;
			head = insert;
		}
		else if (insert == NULL)
		{
			// Insert after the end
			trail -> next = new(LLnode);
			insert = trail -> next;
			*insert = data;
			insert -> next = NULL;
        }
		else
		{
            // General case for insertion
            insert = new(LLnode);
            *insert = data;
            insert -> next = trail -> next;
            trail -> next = insert;
	    }
    }
    return true;
}

// Use the second argument as the input file for the application
int main(int argc, char* argv[])
{
    LLnode *rootNode = NULL;
    LLnode tempData;
    char inputChoice;
    ifstream infile;
    
    tempData.next = NULL;

    if (argc != 2)
    {
        cout << "ERROR: Incorrect number of parameters.\n";
        cout << "Usage: " << argv[0] << " <input filename>\n";
        exit(-1);
    }

    infile.open(argv[1]);

    if (infile.fail())
    {
        cout << "ERROR: Unable to open input file " << argv[1] 
            << "\n";
        exit(-1);
    }
    while (!infile.eof())
    {
        infile >> inputChoice;
        // Based on the input character from the file, do one of the
        // following actions:
        // N - [N]ew Sensor
        // A - List [A]ll Sensors
        // S - Print one Sensor's data
        // U - Update one Sensor's data
        // R - Remove a sensor
        switch (inputChoice)
        {
            case 'N':
            case 'n':
                // New Sensor Case
                // Take in file data into temporary struct,
                // then push it to the linked list.
                infile >> tempData.sensorID;
                infile >> tempData.forest;
                infile >> tempData.temperature;
                infile >> tempData.humidity;
                infile >> tempData.lumens;
                insert(rootNode, tempData);
                break;
            case 'A':
            case 'a':
                // Print all sensors
                printAll(rootNode);
                break;
            case 'S':
            case 's':
                // Print one sensor
                infile >> tempData.sensorID;
                printSensor(rootNode, tempData);
                break;
            case 'U':
            case 'u':
                // Update a certain sensor
                infile >> tempData.sensorID;
                infile >> tempData.temperature;
                infile >> tempData.humidity;
                infile >> tempData.lumens;
                updateSensor(rootNode, tempData);
                break;
            case 'R':
            case 'r':
                // Remove a certain sensor
                infile >> tempData.sensorID;
                remove(rootNode, tempData);
                break;
            default:
                cout << "ERROR: Invalid action in file.\n";
                cout << "Valid actions: (N)ew, Print (A)ll, ";
                cout << "Print (S)ensor, (U)pdate, (R)emove\n";
        } 
    }
    infile.close();
    return 0;
}
