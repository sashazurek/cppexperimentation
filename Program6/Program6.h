/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #6                 *
 * Due Date: March 26th, 2019             *
 * Last Modified: March 23rd, 2019        *
 ******************************************/

const int forestNameLen = 31;

struct LLnode
{
    int sensorID;
    char forest[forestNameLen];
    float temperature;
    float humidity;
    float lumens;
    LLnode *next;
};