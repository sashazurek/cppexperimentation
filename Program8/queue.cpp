/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #8                 *
 * Due Date: April 15th, 2019             *
 * Last Modified: April 15th, 2019        *
 ******************************************/

#include "queue.h"

Queue::Queue()
{
    front = NULL;
    back = NULL;
}

Queue::~Queue()
{
    while (!isEmpty())
    {
	    dequeue();
    }
}
/***********************************************************
 * Function: enqueue 
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To place data inside of a Queue data structure
 * Parameters: newData, of type Appdata
 * Return Value: Nothing
 * Last Modified: April 14th
 **********************************************************/
void Queue::enqueue(Appdata newData)
{
    if (isEmpty())
    {
        // Empty Queue case
        back = front = new(QNode);
        back -> data = newData;
        back -> next = NULL;

    }
    else
    {
        // Normal
        back -> next = new(QNode);
        back = back -> next;
        back -> data = newData;
        back -> next = NULL;
    }
}
/***********************************************************
 * Function: dequeue
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To remove data from the front of the Queue structure
 * Parameters: None
 * Return Value: Data of type Appdata
 * Last Modified: April 12th
 **********************************************************/
Appdata Queue::dequeue()
{
    Appdata data;
    QNode * temp = NULL;

    if (isEmpty())
    {
	    cout << "ERROR: Empty Queue!\n";
    }
    else if (front != back)
    {
        data.copyData(front -> data);
        temp = front;
        front = front -> next;
        delete(temp);
    }
    else
    {
        data.copyData(front -> data);
        delete(front);
        back = front = NULL;
    }
    return(data);
}
/***********************************************************
 * Function: isEmpty
 * Author: Christopher Smith
 * Purpose: To determine if the Queue structure is empty
 * Parameters: None
 * Return Value: Boolean value
 * Last Modified: April 12th
 **********************************************************/
bool Queue::isEmpty()
{
    return(front == NULL);
}

/***********************************************************
 * Function: printAll
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To print all data in the Queue structure to the screen
 * Parameters: None
 * Return Value: None
 * Last Modified: April 12th
 **********************************************************/
void Queue::printAll()
{
    QNode * temp = front;

    cout << "Queue contents:\n";

    while (temp != NULL)
    {
	    temp -> data.printAppData();
	    temp = temp->next;
    }
    cout << "Done.\n";
}

ostream& operator <<(ostream& outs, const Queue& theQueue)
{
    Queue::QNode *temp = theQueue.front;

    outs << "Queue contents:\n";
    while (temp != NULL)
    {
        temp -> data.printAppData();
	    temp = temp->next;
    }
    outs << "Done.\n";

    return(outs);
}


