/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #8                 *
 * Due Date: April 15th, 2019             *
 * Last Modified: April 13th, 2019        *
 ******************************************/

#include <iostream>
#include "Appdata.h"
#include "queue.h"

int main()
{
    Appdata temp;
    Queue myQueue;
    double tempTemperature, tempHumidity;
    char inputChar;

    // Application will read user input to do action
    // E - Enqueue supplied data
    // D - Dequeue and print dequeued data
    // P - Print entire queue
    // I - Check if queue is empty
    // Q - Quit application after destroying queue

    do
    {
        cout << "\nE - Add new data to the queue\n";
        cout << "D - Fetch the next piece of data in the queue\n";
        cout << "P - Dump the contents of the queue\n";
        cout << "I - Check if the queue is empty\n";
        cout << "Q - Quit the program\n";
        cout << "Choice? ";
        cin >> inputChar;
        
        switch (inputChar)
        {
            case 'E':
                // E - Enqueue supplied data
                cout << "Input Temperature: ";
                cin >> tempTemperature;
                cout << "Input Humidity: ";
                cin >> tempHumidity;
                if (temp.setData(tempTemperature,tempHumidity))
                {
                    myQueue.enqueue(temp);
                }
                break;
            case 'D':
                // D - Dequeue and print dequeued data
                cout << myQueue.dequeue();
                break;
            case 'P':
                // P - Print entire queue
                myQueue.printAll();
                break;
            case 'I':
                // I - Check if queue is empty
                if (myQueue.isEmpty() == 0)
                {
                    cout << "Queue is not empty!\n";
                }
                else
                {
                    cout << "Queue is empty!\n";
                }
                break;
            case 'Q':
                // Q - Quit application after destroying queue
                myQueue.~Queue();
                break;
            default:
                cout << "ERROR: Supplied char is not valid input.\n";
                break;
        }
    } while (inputChar != 'Q');
    
    return(0);

}
