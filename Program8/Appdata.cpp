/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #8                 *
 * Due Date: April 15th, 2019             *
 * Last Modified: April 15th              *
 ******************************************/

#include "Appdata.h"

Appdata::Appdata()
{
    temperature = -1000.0;
    humidity = -1.0;
}

Appdata::~Appdata()
{
}
/***********************************************************
 * Function: setData
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To put data inside of an Appdata structure
 * Parameters: newData of type char
 * Return Value: None
 * Last Modified: April 15th
 **********************************************************/
bool Appdata::setData(double temp, double humid)
{
    if (humid <= 1.0 && humid >= 0.0)
    {
        this -> humidity = humid; 
        this -> temperature = temp; 
        return true;
    }
    else
    {
        cout << "ERROR: Humidity must be decimal between 0 and 1\n";
        return false;
    }
}
/***********************************************************
 * Function: getData 
 * Author: Michael Zurek
 * Purpose: To access data inside of an Appdata structure
 * Parameters: None
 * Return Value: Two variables of type float
 * Last Modified: April 12th
 **********************************************************/
double Appdata::getData()
{
    return(temperature,humidity);
}
/***********************************************************
 * Function: copyData
 * Author: Michael Zurek
 * Purpose: To correctly copy data between Appdata variables
 * Parameters: None
 * Return Value: None
 * Last Modified: April 12th
 **********************************************************/
void Appdata::copyData(Appdata data)
{
    this -> temperature = data.temperature;
    this -> humidity = data.humidity;
}
/***********************************************************
 * Function: printAppData
 * Author: Michael Zurek
 * Purpose: To print data contained in an Appdata structure
 * Parameters: None
 * Return Value: None
 * Last Modified: April 12th
 **********************************************************/
void Appdata::printAppData()
{
    cout << "\tTemperature: " << temperature << endl;
    cout << "\tHumidity: " << humidity << endl;
}

ostream& operator <<(ostream& outs, const Appdata& theAppdata)
{
    outs << "Temperature: " << theAppdata.temperature << endl;
    outs << "Humidity: " << theAppdata.humidity << endl;

    return(outs);
}