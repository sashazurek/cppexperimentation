/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #8                 *
 * Due Date: April 15th, 2019             *
 * Last Modified: April 15th, 2019        *
 ******************************************/

#include <iostream>
#include <fstream>

using namespace std;

/***********************
 * Class Name: Appdata
 * Purpose: Establish an agnostic data structure for Queue 
 * Last Modified: April 15th
 ***********************/
#ifndef APPDATA
#define APPDATA

class Appdata
{
    public:
        Appdata();
        ~Appdata();
        void copyData(Appdata data);
        bool setData(double temp, double humid);
        double getData();
        void printAppData();
        friend ostream& operator <<(ostream& outs, const Appdata& theAppdata);

    private:
        double temperature, humidity;
};
#endif
