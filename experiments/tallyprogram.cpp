// This code sucks and is not very reusable
// Supposed to answer Challenge #361
// Only works when input is styled like "abcdeabcde"
// Breaks when using pseudorandom entries
// Also overfills reqs; would work on more than 5 friends
// Also does not meet reqs; does not handle losing points
// Maybe come back to this again some other time

#include <iostream>

using namespace std;

struct Player
{
    char playerName;
    int tally;
};

int main()
{
    int x,y,unique;
    string input, track;
    Player * tallies;

    cout << "Enter the stream for tallying: ";
    cin >> input;

    for (x = 0; x < input.length(); x++)
    {
        if (track.find(input[x]) == string::npos)
            track = track + input[x];
    }

    tallies = new Player[track.length()];
    unique = track.length();

    track = "";

    for (x = 0; x < input.length(); x++)
    {
        if (track.find(input[x]) == string::npos)
        {
            tallies[x].playerName = input[x];
            tallies[x].tally++;
            track = track + input[x];
        }
        else
        {
            for (y = 0; y < unique; y++)
                if (tallies[y].playerName == input[x])
                    tallies[y].tally++;
        }
        
    }

    for (x = 0; x < track.length(); x++)
        cout << tallies[x].playerName << tallies[x].tally << endl;

    return 0;
}
