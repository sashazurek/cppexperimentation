/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #7                 *
 * Due Date: April 4th, 2019              *
 * Last Modified: April 2nd 17:12         *
 ******************************************/

#include <iostream>
#include "Appdata.h"
#include "stack.h"

int main(int argc, char* argv[])
{
    Appdata temp;
    Stack myStack;
    char inputChar;
    ifstream infile;

    if (argc != 2)
    {
        cout << "ERROR: Incorrect number of parameters.\n";
        cout << "Usage: " << argv[0] << " <input filename>\n";
        exit(-1);
    }

    infile.open(argv[1]);

    if (infile.fail())
    {
        cout << "ERROR: Unable to open input file " << argv[1] 
            << "\n";
        exit(-1);
    }

    // Read from the file and put characters in the stack
    // Use bottom checking loop to end upon reaching end-of-line char
    infile >> noskipws >> inputChar;
    do
    {
        temp.setData(inputChar);
        myStack.push(temp);
        infile >> noskipws >> inputChar;
        if (inputChar == '^')
        {
            while (!myStack.isEmpty())
            {
                cout << myStack.pop();
            }
            infile >> noskipws >> inputChar;
            cout << endl;
        }
    } while (!infile.eof());

    infile.close();
    return(0);

}
