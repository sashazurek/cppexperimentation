/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #7                 *
 * Due Date: April 4th, 2019              *
 * Last Modified: April 4th, 2019         *
 ******************************************/

#include "Appdata.h"

Appdata::Appdata()
{
    data = '\0';
}

Appdata::~Appdata()
{
}
/***********************************************************
 * Function: setData
 * Author: Christopher Smith
 * Purpose: To put data inside of an Appdata structure
 * Parameters: newData of type char
 * Return Value: None
 * Last Modified: April 4th, 16:06
 **********************************************************/
void Appdata::setData(char newData)
{
   this->data = newData; 
}
/***********************************************************
 * Function: getData 
 * Author: Christopher Smith
 * Purpose: To access data inside of an Appdata structure
 * Parameters: None
 * Return Value: Data of type char
 * Last Modified: April 4th, 16:08
 **********************************************************/
char Appdata::getData()
{
    return(data);
}
/***********************************************************
 * Function: getData 
 * Author: Christopher Smith
 * Purpose: To print data contained in an Appdata structure
 * Parameters: None
 * Return Value: None
 * Last Modified: April 4th, 16:09
 **********************************************************/
void Appdata::printAppData()
{
    cout << data;
}

ostream& operator <<(ostream& outs, const Appdata& theAppdata)
{
    outs << theAppdata.data;

    return(outs);
}

