/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #7                 *
 * Due Date: April 4th, 2019              *
 * Last Modified: March 29th, 2019        *
 ******************************************/

#include <iostream>
#include <fstream>

using namespace std;

/***********************
 * Class Name: Appdata
 * Purpose: Establish an agnostic data structure for Stack 
 * Last Modified: April 4th, 2019
 ***********************/
#ifndef APPDATA
#define APPDATA

class Appdata
{
    public:
        Appdata();
        ~Appdata();
        void setData(char newData);
        char getData();
        void printAppData();
        friend ostream& operator <<(ostream& outs, const Appdata& theAppdata);

    private:
        char data;
};
#endif
