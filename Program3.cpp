/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program 3                *
 * Due Date: February 6th, 2019         *
 * Last Modified: February 1st 17:33    *
 ****************************************/

#include <iostream>
#include <cmath>

using namespace std;

bool checkPrime(long theNum)
{
    // Check if input is even, return false if true
    if (theNum % 2 == 0)
        return false;
    // Find the square root of theNum
    // Cast (theNum)^1/2 to a long, round it
    long sqrtTheNum;
    sqrtTheNum = (long) sqrt(theNum);
    // if a value from 3 to (theNum^1/2) evenly divides, return false
    for (int i = 3; i < (sqrtTheNum+1); i += 2)
    {
        if (theNum % i == 0)
            return false;
    }
    // Otherwise, return true
    return true;
}

int main() 
{
    // Use a do-while loop for an interactive menu
    // that asks for a number to check
    // Use -1 as the exit parameter
    long inputNumber;
    bool theResult;

    do
    {
        cout << "Enter your potential prime (-1 to end): ";
        cin >> inputNumber;
        if (inputNumber != -1)
        {
            theResult = checkPrime(inputNumber);
            if (theResult)
                cout << "Prime" << endl;
            else
                cout << "Not Prime" << endl;
        }
    } while (inputNumber != -1);
    return 0;
}
