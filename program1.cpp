/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program #1               *
 * Due Date: January 15th, 2019         *
 * Last Modified: January 10th, 2019    *
 ****************************************/

#include <iostream>
using namespace std;
int main() 
{
    int meterInput;
    cout << "Enter the distance in Meters: ";
    cin >> meterInput;
    cout << "The equivalent distances to " << meterInput << " meters are:" << endl;
    // Using 1000.0 instead of 1000 so that the output is a float
    cout << (meterInput / 1000.0) << " Kilometers " << endl;
    cout << (meterInput * 100) << " Centimeters " << endl;
    cout << (meterInput * 1000) << " Millimeters " << endl;
    return 0;
}
