// This is a template for a simple stack data structure.

#include <iostream>

using namespace std;

struct stackData
{
    int data;
    stackData * down;
};

void place(stackData* &head, stackData data)
{
    stackData * place = head;

    if (place == NULL)
    {
        // Empty case
        head = new(stackData);
        *head = data;
        head -> down = NULL;
    }
    else
    {
        // General case; place on top of stack
        place = new(stackData);
        *place = data;
        place -> down = head;
    }
}

void printAll(stackData* &head)
{
    stackData* hike = head;

    while (hike != NULL)
    {
        cout << hike -> data << endl;
        hike = hike -> down;
    }
    return;
}

int main() 
{
    stackData inputData;
    char userChoice;
    stackData * base = NULL;

    inputData.down = NULL;

    cout << "Intializing the stack, insert data: ";
    cin >> inputData.data;
    place(base, inputData);

    cout << "What would you like to do? ";
    while (true);
    {
        cin >> userChoice;
        switch (userChoice)
        {
            case 'A':
            case 'a':
                cout << "Enter data: ";
                cin >> inputData.data;
                place(base, inputData);
                break;
            case 'P':
            case 'p':
                printAll(base);
                break;
            case 'R':
            case 'r':
                // Remove
                break;
            case 'Q':
            case 'q':
                return 0;
                break;
            default:
                cout << "Invalid input." << endl;
        }
    }
    return 0;
}
